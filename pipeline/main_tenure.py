from src.helpers.data import DataLoader
from src.models.model_tenure import SurvivalModel

from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error, mean_absolute_percentage_error

COL_CHURN_FLAG = "CHURN_FLAG"
COL_Y_SURVIVAL_TIME = "Y_SURVIVAL_TIME"
COL_RAND = "RAND_NUM"
COL_TENURE_PRED = "TENURE_PREDICTION"


X_COLUMNS = [
    "NUM_PRICE_INCREASES",
    "NUM_PRICE_DECREASES",
    "NUM_UPSELLS",
    "NUM_DOWNSPINS",
    "DAYS_SINCE_LAST_UPSELL",
    "DAYS_SINCE_LAST_DOWNSPIN",
    "NUM_CONTRACTS",
    "NUM_COMPETITORS_IN_POSTCODE",
    "FLAG_BT_IN_POSTCODE",
    "FLAG_TALKTALK_IN_POSTCODE",
    "FLAG_SKY_IN_POSTCODE",
    "FLAG_HYPEROPTIC_IN_POSTCODE",
    "AVERAGE_MONTHLY_COST",
    COL_Y_SURVIVAL_TIME,
    COL_CHURN_FLAG
]


def main():
    model = SurvivalModel()
    data_loader = DataLoader()

    df = data_loader.get_training_data(file_name="prc_contract_churn")
    df = df.dropna(axis=0, subset=X_COLUMNS)
    df_train = df[df[COL_RAND] < 0.8]
    df_test = df[df[COL_RAND] >= 0.8]

    model.fit(df_train[X_COLUMNS], duration_col=COL_Y_SURVIVAL_TIME, event_col=COL_CHURN_FLAG)
    df_test = model.predict_tenure(df_test)
    y_true = df_test[COL_Y_SURVIVAL_TIME]
    y_pred = df_test[COL_TENURE_PRED]

    print(f"mse: {mean_squared_error(y_true, y_pred)}")
    print(f"r2: {r2_score(y_true, y_pred)}")
    print(f"mae: {mean_absolute_error(y_true, y_pred)}")
    print(f"mape: {mean_absolute_percentage_error(y_true, y_pred)}")


if __name__ == "__main__":
    main()
