from src.helpers.data import DataLoader

from lifetimes.utils import summary_data_from_transaction_data
from lifetimes import BetaGeoFitter, GammaGammaFitter


class PipelineRunner:
    COL_CUSTOMER_ID = "CUSTOMER_ID"
    COL_PAYMENT_DATE = "PAYMENT_DATE"
    COL_PAYMENT_AMOUNT = "PAYMENT_AMOUNT"

    COL_FREQUENCY = "frequency"
    COL_RECENCY = "recency"
    COL_P_ALIVE = "probability_alive"
    COL_T = "T"
    COL_MONETARY_VAL = "monetary_value"
    COL_PREDICTED_CLV = "predicted_clv"

    INPUT_DATA_FILE = "fe_transactions"

    def __init__(self):
        self.data_loader = DataLoader()

    def run(self):
        """
        Run the CLV model pipeline
        """
        df = self.data_loader.get_training_data(file_name=self.INPUT_DATA_FILE).head(1_000_000)  # 1m rows
        summary = summary_data_from_transaction_data(df,
                                                     self.COL_CUSTOMER_ID,
                                                     self.COL_PAYMENT_DATE,
                                                     self.COL_PAYMENT_AMOUNT).reset_index()
        bgf = BetaGeoFitter(penalizer_coef=0.001)
        bgf.fit(summary[self.COL_FREQUENCY], summary[self.COL_RECENCY], summary[self.COL_T])
        summary[self.COL_P_ALIVE] = bgf.conditional_probability_alive(summary[self.COL_FREQUENCY],
                                                                      summary[self.COL_RECENCY],
                                                                      summary[self.COL_T])
        summary = summary[summary[self.COL_MONETARY_VAL] > 0]

        ggf = GammaGammaFitter(penalizer_coef=0.001)
        ggf.fit(summary[self.COL_FREQUENCY], summary[self.COL_MONETARY_VAL])

        summary[self.COL_PREDICTED_CLV] = ggf.customer_lifetime_value(bgf,
                                                                      summary[self.COL_FREQUENCY],
                                                                      summary[self.COL_RECENCY],
                                                                      summary[self.COL_T],
                                                                      summary[self.COL_MONETARY_VAL],
                                                                      time=24,
                                                                      freq="M")
        print(summary)

    def evaluate_model(self):
        """
        Evaluate the CLV model
        :return:
        """
        evaluation_ggf = "evaluation_ggf"
        evaluation_churn = "evaluation_churn"
        df = self.data_loader.get_training_data(file_name=evaluation_ggf)

        # Split training / test data - train on data prior to x, evaluate on data after y
        df_test = df
        df_train = df

        train_summary = summary_data_from_transaction_data(df,
                                                           self.COL_CUSTOMER_ID,
                                                           self.COL_PAYMENT_DATE,
                                                           self.COL_PAYMENT_AMOUNT).reset_index()

        bgf = BetaGeoFitter(penalizer_coef=0.001)
        bgf.fit(train_summary[self.COL_FREQUENCY],
                train_summary[self.COL_RECENCY],
                train_summary[self.COL_T])
        train_summary[self.COL_P_ALIVE] = bgf.conditional_probability_alive(train_summary[self.COL_FREQUENCY],
                                                                            train_summary[self.COL_RECENCY],
                                                                            train_summary[self.COL_T])
        train_summary = train_summary[train_summary[self.COL_MONETARY_VAL] > 0]

        ggf = GammaGammaFitter(penalizer_coef=0.001)
        ggf.fit(train_summary[self.COL_FREQUENCY], train_summary[self.COL_MONETARY_VAL])

        train_summary[self.COL_PREDICTED_CLV] = ggf.customer_lifetime_value(bgf,
                                                                            train_summary[self.COL_FREQUENCY],
                                                                            train_summary[self.COL_RECENCY],
                                                                            train_summary[self.COL_T],
                                                                            train_summary[self.COL_MONETARY_VAL],
                                                                            time=24,
                                                                            freq="M")


if __name__ == "__main__":
    pipeline_runner = PipelineRunner()
    pipeline_runner.run()
