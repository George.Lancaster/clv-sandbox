import lifelines
import lifetimes

from src.models.model_tenure import SurvivalModel
from lifetimes.fitters.gamma_gamma_fitter import GammaGammaFitter


class ClvModel:

    """
    Class for the entire CLV model.
    This model is made up of two separate models:
        1. A Cox PH model for the Survival part of the prediction (how long do we think customers are going stay)
        2. GammaGamma model that tells us how much each customer will spend over their predicted tenure
    """

    def __init__(self):
        self.survival_model = SurvivalModel()
        self.gamma_model = GammaGammaFitter()

    def predict(self):
        pass

    def fit(self, data_survival, data_clv):
        self.survival_model.fit(data_clv)
        self.gamma_model.fit(data_clv)



