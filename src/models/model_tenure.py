from pathlib import Path
import joblib as joblib
import pandas as pd
from typing import List
from lifelines import CoxPHFitter
from os.path import exists, join
from src.helpers.root import root
from pandas import DataFrame, Series


class SurvivalModelException(Exception):
    pass


class SurvivalModel:

    def __init__(self):
        model_path: str = join(root(), "resources", "", "model.joblib")
        self.fitted: bool = False

        if exists(model_path):
            self.model_artifact = joblib.load(model_path)
            self.fitted = True
        else:
            self.model_artifact = CoxPHFitter()

    def predict_tenure(self, data_x: DataFrame):
        """
        Predict tenure for each customer in data_x and append the prediction to the DataFrame
        :param data_x: DataFrame of customer data
        :return: data_x`
        """
        customer_tenures = []
        survival_functions = self.model_artifact.predict_survival_function(data_x)
        for index in data_x.index:
            survival_function = survival_functions[index].to_list()
            tenure_list = [x for x in survival_function if x > 0.5]
            if len(tenure_list) == 0:
                days_in_future = 0
            else:
                days_in_future = survival_function.index(min(tenure_list))
            customer_tenures.append(days_in_future)
        data_x["TENURE_PREDICTION"] = customer_tenures
        return data_x

    def evaluate(self, df_train, df_test, duration_col: str = "Y_SURVIVAL_TIME", event_col: str = "CHURN_FLAG"):
        if not self.fitted:
            self.fit(df_train, duration_col=duration_col, event_col=event_col)

        # df_p = df_test.apply(lambda x: self.model_artifact)
        # self.model_artifact.
        # df_pred = self.predict_proba(df_test)
        print()

    def fit(self, data_x: DataFrame, duration_col: str = "Y_SURVIVAL_TIME", event_col: str = "CHURN_FLAG") -> None:
        self.model_artifact.fit(data_x, duration_col=duration_col, event_col=event_col, show_progress=True)
        self.model_artifact.print_summary()
        self.fitted = True
