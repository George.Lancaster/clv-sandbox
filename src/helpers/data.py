from google.auth import load_credentials_from_file
from google.cloud.bigquery import Client

from src.helpers.root import root
from os.path import join
import pandas as pd
from pandas import DataFrame


class FileIOError(Exception):
    """
        Exception for the FileIO class
    """


class FileIO:

    """
        Class to handle file Input / Output.
    """

    PATH_RESOURCES: str = "resources"
    PATH_SQL: str = "sql"
    PATH_CSV: str = "csv"

    SUFFIX_SQL: str = ".sql"
    SUFFIX_CSV: str = ".csv"

    FILE_CONTRACT_CHURN: str = "prc_contract_churn"

    def get_sql(self, file_name: str) -> str:
        """
        Retrieve a sql file locally
        :param file_name: The name of the file to get, without a file extension.
        :return: Contents of file_name as a string.
        """
        with open(self._get_file_path(file_name, self.PATH_SQL), "r") as open_file:
            return open_file.read()

    def get_csv(self, file_name: str) -> DataFrame:
        """
        Retrieve a csv file locally
        :param file_name: The name of the file to get, without a file extension.
        :return: Contents of file_name as a Pandas DataFrame
        """
        file_path = self._get_file_path(file_name, self.PATH_CSV)
        return pd.read_csv(file_path)

    def put_csv(self, csv_file: DataFrame, file_name: str, verbose: bool=False) -> None:
        """
        Write a csv file locally.
        :param csv_file: The contents of the csv file to write.
        :param file_name: The name of the file to write, excluding a file extension.
        """
        file_path = self._get_file_path(file_name, self.PATH_CSV)
        csv_file.to_csv(file_path)

    def _get_file_path(self, file_name: str, file_type: str) -> str:
        """
        Get the absolute path to a file. If invalid file_type is given, then throw an exception.
        :param file_name: The name of the file to get, excluding a file extension.
        :param file_type: The type of file to get, one of: ["csv", "sql"]
        :return: Absolute path to a file as a string.
        """
        if file_type == self.PATH_SQL:
            suffix = self.SUFFIX_SQL
            path_name = self.PATH_SQL
        elif file_type == self.PATH_CSV:
            suffix = self.SUFFIX_CSV
            path_name = self.PATH_CSV
        else:
            raise FileIOError("Invalid file_type argument. Must be one of (sql, csv)")

        return join(root(), self.PATH_RESOURCES, path_name, f"{file_name}{suffix}")


class DataLoader:

    """
        Class to load data from both local storage and cloud.
    """

    DEFAULT_PROJECT: str = "prj-vo-aa-p-ds-customer-uat"
    DEFAULT_DATASET: str = "clv"

    def __init__(self):
        self.file_io = FileIO()

    def _download_save_data(self, file_name: str, verbose: bool):
        """
        Private method to download data from BigQuery and save locally.
        :param file_name: The name of the .sql file, also the name of the file that is saved locally
        (excluding a file extension).
        :return: Pandas DataFrame containing the loaded data.
        """
        if verbose:
            print(f"Downloading {file_name}...")
        sql = self.file_io.get_sql(file_name)
        bq_client = Client()
        load_credentials_from_file("/Users/l7904582/.config/gcloud/application_default_credentials.json")
        df_train = bq_client.query(sql).to_dataframe()
        self.file_io.put_csv(df_train, file_name, verbose)
        return df_train

    def get_training_data(self, download: bool = False, file_name: str = FileIO.FILE_CONTRACT_CHURN, verbose:bool = True) -> DataFrame:
        """
        Try to retrieve data locally, if file not found, download from BigQuery and save locally.
        :param download: If true, don't look for data locally.
        :param file_name: The name of the file to load (excluding a file extension).
        :return: Pandas DataFrame containing the loaded data.
        """
        if download:
            if verbose:
                print(f"Downloading and saving")
            df_train = self._download_save_data(file_name, verbose)
        else:
            try:
                df_train = self.file_io.get_csv(file_name)
            except FileNotFoundError:
                df_train = self._download_save_data(file_name, verbose)
        return df_train
