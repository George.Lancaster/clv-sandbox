import os
from pathlib import Path


def root():
    return str(Path(os.path.dirname(os.path.realpath(__file__))).parent.parent.absolute())
